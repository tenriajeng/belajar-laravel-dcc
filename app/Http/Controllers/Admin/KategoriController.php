<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function __construct()
    {
        $this->title = 'kategori';
    }

    public function index()
    {
        $data = Kategori::all();
        return view('admin.kategori.index', compact('data'));
    }
    public function create()
    {
        return view('admin.kategori.create');
    }

    public function store(Request $request)
    {
        $model = $request->all();
        $model['user_id'] = auth()->user()->id;
        $data = Kategori::create($model);
        if($data){
            Alert::toast('Data Berhasil Disimpan', 'success');
        }else{
            Alert::toast('Data Berhasil Disimpan', 'danger');
        }
        return redirect('admin/kategori');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data = Kategori::find($id);
        return view('admin.kategori.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $model = $request->all();
        $data = Kategori::find($model['id']);
        $model['user_id'] = auth()->user()->id;
        if($data->update($model)){
            Alert::toast('Data Berhasil Diupdate', 'success');
        }else{
            Alert::toast('Data Berhasil Diupdate', 'danger');
        }
        return redirect('admin/kategori');
    }

    public function destroy(Request $request, $id)
    {
        $data = Kategori::find($id);
        if($data->delete()){
            Alert::toast('Data Berhasil Diupdate', 'success');
        }else{
            Alert::toast('Data Berhasil Diupdate', 'danger');
        }
        return redirect('admin/kategori');
    }
}
